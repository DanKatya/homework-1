//
//  UIColor+HWExtension.h
//  HelpWill
//
//  Created by Viktor Peschenkov on 05.07.16.
//  Copyright © 2016 INOSTUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>

#define HWFRGB(__fvalue__) ((__fvalue__)/255.0f)

@interface UIColor (HWExtension)

+ (instancetype)hw_colorWithHexInt:(NSUInteger)hexColor;

+ (instancetype)hw_colorWithHexInt:(NSUInteger)hexColor alpha:(CGFloat)alpha;

+ (instancetype)hw_randomColor;

@end
