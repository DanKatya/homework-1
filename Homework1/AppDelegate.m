//
//  AppDelegate.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 20/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "AppDelegate.h"
#import "HWHomeworkViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [[HWHomeworkViewController alloc] init];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
