//
//  HWMatrixView.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWMatrixView : UIView

@property (nonatomic, strong, readonly) UITextField *rowsCountInputTextFiled;
@property (nonatomic, strong, readonly) UITextField *columnsCountInputTextField;
@property (nonatomic, strong, readonly) UIButton *createButton;
@property (nonatomic, strong, readonly) UIScrollView *scrollView;

@property (nonatomic, strong, readonly) UILabel *emptyLabel;

@property (nonatomic, assign) UIEdgeInsets contentInsets;

- (void)setMatrix:(NSArray<NSArray<NSNumber *> *> *)matrix;

@end
